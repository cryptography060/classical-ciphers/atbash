############Monoalphabetic Substitution############
'''
Description: Atbash encrypter and decrypter
Author: Mogons
Usage:
'''
import string

def remove_ws(strng): 
    return strng.translate({ord(c): None for c in string.whitespace})

def wrong():
    print("Wrong Input!")

def exit():
    raise SystemExit(0)

def atbash(code):
    
    alphabet_plain = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

    # Atbash cipher alphabet
    # Reverse the alphabet list using slice notation
    alphabet_cipher = alphabet_plain[::-1]

    print("Note: No numbers/symbols allowed.\nAll letters will be capitalized.\nWhitespace will be removed.\nType exit to quit the program.")
    i = False
    while i == False:
        message = input("What would you like to "+code+"?").upper()
        #print(message)
        message = remove_ws(message)
        print("Input: "+message)

        for character in message:
            if character not in alphabet_plain:
                wrong()
                i = False
                break
            else:
                i = True

        if message == 'EXIT':
            exit()

    text = ""
    for character in message:
       text += alphabet_plain[-(alphabet_plain.index(character)+1)]

    return text

def atbash_encode():
    code = "encode"

    ciphertext = atbash(code)

    print("Ciphertext: "+ciphertext)



def atbash_decode():
    code = "decode"
    plaintext = atbash(code)
    print("Plaintext: "+plaintext)

def switch(arg):
    switcher = {
        'encode': atbash_encode,
        'decode': atbash_decode,
        'exit': exit,
        }
    func = switcher.get(arg, lambda: wrong())
    
    return func()

if __name__=="__main__":

    while True:
        task = input("encode/decode/exit?")
        print(task)
        switch(task)
